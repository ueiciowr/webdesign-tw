
(() => {
  window.addEventListener("load", () => {
    const dateEl = document.querySelector(".date");
    const year = new Date().getFullYear();
    dateEl.innerText = `  ${year}`;
  });
 })()
